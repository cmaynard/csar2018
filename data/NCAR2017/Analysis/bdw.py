#!/Library/Frameworks/Python.framework/Versions/2.7/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys

def plotData(x,y):
    fig, ax = plt.subplots()
    width = 0.1*x
    rects = ax.bar(x,y,width, label='knl')
    ax.set_ylim(0,1.1*y[0])
    ax.set_xlim(0.9,1.5*x[len(x)-1])
    ax.set_xscale('log')
    ax.set_xticks(x)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
    ax.set_xlabel("OMP threads")
#    ax.set_ylabel("speed up versus single thread")
    ax.set_ylabel("time in seconds")
    ax.legend(loc='upper left')
    plt.show()

def plot3Data(x,y1,y2,y3):
    fig, ax = plt.subplots()
    width = 0.1*x
    rects1 = ax.bar(0.9*x,y1,width, color='sandybrown',label='No Opts')
    rects2 = ax.bar(x,y2,width,color='indigo', label='SIMD Opts')
    rects3 = ax.bar(1.1*x,y3,width,color='teal', label='ivdep Opts')
    ax.set_ylim(0,1.1*y1[0])
    ax.set_xlim(0.7,1.5*x[len(x)-1])
    ax.set_xscale('log')
    ax.set_xticks(x)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
    ax.set_xlabel("OMP threads")
#    ax.set_ylabel("speed up versus single thread")
    ax.set_ylabel("time in seconds")
    ax.legend(loc='upper right')
    plt.show()

def plot2Data(x,y1,y2):
    fig, ax = plt.subplots()
    width = 0.1*x
    rects1 = ax.bar(0.9*x,y1,width, color='indigo',label='No Opts')
    rects2 = ax.bar(x,y2,width,color='lightcoral', label='Opts')
    ax.set_ylim(0,1.1*y2[0])
    ax.set_xlim(0.7,1.5*x[len(x)-1])
    ax.set_xscale('log')
    ax.set_xticks(x)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
    ax.set_xlabel("OMP threads")
#    ax.set_ylabel("speed up versus single thread")
    ax.set_ylabel("time in seconds")
    ax.legend(loc='upper right')
    plt.show()        

def readData(fname):
    data = np.genfromtxt(fname, skip_header=0, usecols=range(0,2))
    threads = np.array(data[:,0])
    time = np.array(data[:,1])
    return threads, time
    
bstem="../isambard/BDW/"
#fo=sys.argv[1]
#fname=fstem+fo
#fname=mstem+"LO_noopt_pp.dat"
fname=bstem+"LO_noopt_pp.dat"
x1,y1 = readData(fname)
print y1
fname=bstem+"LO_opt_pp.dat"

x2,y2 = readData(fname)
print y2
#fname=mstem+"LO_opt_cray_pp.dat"
#fname=kstem+"LO_opt_ivdep_pp.dat"

#x3,y3 = readData(fname)


#plot3Data(x1,y1,y2,y3)
plot2Data(x1,y1,y2)

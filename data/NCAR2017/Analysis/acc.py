#!/Library/Frameworks/Python.framework/Versions/2.7/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys


#fstem="../isambard/ACC/"
fstem=""
fo=sys.argv[1]
fname=fstem+fo
Ldata = np.genfromtxt(fname, skip_header=0, usecols=range(0,1))
g=Ldata.sum()
c=Ldata[0:4].sum()
dif=g-c
frac=dif/g
#print str(g) + " " +str(c)+ " " + str(dif) + " " + str(frac)
print g

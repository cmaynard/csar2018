#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import sys

def plot2Data(y1 ,y2, y3, y4, xall, fstem):
    fig, ax = plt.subplots()

    line1 = ax.plot(xall, y1, '-o', color='royalblue',label="YAXT 10 timesteps",linewidth=3.0)
    line2 = ax.plot(xall, y2, '-o', color='firebrick',label="ESMF 10 timesteps",linewidth=3.0)
    line3 = ax.plot(xall, y3, '-o', color='seagreen',label="YAXT Routing tables",linewidth=3.0)
    line4 = ax.plot(xall, y4, '-o', color='orange',label="ESMF Routing tables",linewidth=3.0)

    ax.legend(loc='upper right')

    ax.set_xlabel("Number of PEs")
    ax.set_ylabel("Time (s)")

    ax.set_axisbelow(True)
    ax.grid(b=True, which='major', color='lightgray', linestyle='-')

    fname=fstem+".png"
    plt.savefig(fname,format="png")
    fname=fstem+".eps"
    plt.savefig(fname,format="eps")    
    plt.show()

pes = np.array(range(0,3))
pes[0] = 216
pes[1] = 864
pes[2] = 3456

yaxt10dt = np.array(range(0,3))
yaxt10dt[0] = 1006.06
yaxt10dt[1] = 260.90
yaxt10dt[2] = 72.10

yaxtroute = np.array(range(0,3))
yaxtroute[0] = 9.69
yaxtroute[1] = 6.47
yaxtroute[2] = 7.18

esmf10dt = np.array(range(0,3))
esmf10dt[0] = 1023.84
esmf10dt[1] = 272.78
esmf10dt[2] = 78.71

esmfroute = np.array(range(0,3))
esmfroute[0] = 90.62
esmfroute[1] = 85.37
esmfroute[2] = 63.26

plot2Data(yaxt10dt, esmf10dt, yaxtroute, esmfroute, pes,"YAXT-ESMF_strong_scaling")

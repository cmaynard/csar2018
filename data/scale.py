#!/usr/local/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys

def plot2Data(x1, y1 ,x2, y2, xall, fstem):
    fig, ax = plt.subplots()
    xlim=[180,6500]
    ax.set_xlim(xlim[0],xlim[1])
    width1=0.1*x1
    width2=0.1*x2
    rects1 = ax.bar(x1, y1, width1, color='firebrick',label="LFRic",edgecolor='black')
    rects2 = ax.bar(x2, y2, width2, color='royalblue',label="UM",edgecolor='black')
    ax.legend(loc='upper right')
    ax.set_xscale('log')
#    xs=np.concatenate(x1,x2)
#    print xs
    ax.set_xticks(xall)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())

    
#    maxy = 1.1*max(m)
#    ax.set_ylim(0, maxy)
    ax.set_xlabel("XC40 nodes")
    ax.set_ylabel("Parallel efficiency")

    ax.annotate('157464 cores', xy=(4374,0.7), xytext=(1200,0.9), arrowprops=dict(facecolor='black', shrink=0.05),            )
    fname=fstem+".png"
    plt.savefig(fname,format="png")
    fname=fstem+".eps"
    plt.savefig(fname,format="eps")    
    plt.show()


lname="lfric.dat"
ldata=np.genfromtxt(lname, skip_header=0, usecols=range(0,2))

uname="umg.dat"
udata=np.genfromtxt(uname, skip_header=0, usecols=range(0,2))

lnodes=np.array(ldata[:,0])
ltime=np.array(ldata[:,1])

unodes=np.array(udata[:,0])
utime=np.array(udata[:,1])

lsn=np.array(lnodes[:]/lnodes[0])
lst=np.array(ltime[0]/lsn[:])

lpe=np.array(lst[:]/ltime[:])

usn=np.array(unodes[:]/unodes[0])
ust=np.array(utime[0]/usn[:])
upe=np.array(ust[:]/utime[:])

#n=len(lnodes)+len(unodes)
#nodes=np.array(range(0,n))
#for i in range(0,len(lnodes)):
#    nodes[i]=lnodes[i]

#for i in range(len(lnodes),len(lnodes)+len(unodes)):
#    nodes[i]=unodes[i-len(lnodes)]

nodes = np.array(range(0,6))
nodes[0] = 200
nodes[1] = 400
nodes[2] = 800
nodes[3] = 1600
nodes[4] = 3200
nodes[5] = 6400

print nodes

plot2Data(lnodes, lpe, unodes, upe, nodes,"LFRIcVsUM")


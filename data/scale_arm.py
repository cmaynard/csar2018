#!/usr/local/bin/python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import sys

def plot2Data(x1, y1 ,x2, y2, fstem):
    fig, ax = plt.subplots()
    xlim=[0.5,40]
    ax.set_xlim(xlim[0],xlim[1])
    width1=0.1*x1
    width2=0.1*x2
    rects1 = ax.bar(1.05*x1, y1, width1, color='orange',label="No Opt",edgecolor='black')
    rects2 = ax.bar(0.95*x2, y2, width2, color='seagreen',label="Opt",edgecolor='black')
    ax.legend(loc='upper right')
    ax.set_xscale('log')
#    xs=np.concatenate(x1,x2)
#    print xs
    ax.set_xticks(x1)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
    ax.set_xlabel("OMP threads")
    ax.set_ylabel("Parallel Efficiency")
    fname=fstem+".png"
    plt.savefig(fname,format="png")
    fname=fstem+".eps"
    plt.savefig(fname,format="eps")    

    plt.show()

def plotData(x, y):
    fig, ax = plt.subplots()
    xlim=[0.5,40]
    ax.set_xlim(xlim[0],xlim[1])
    width=0.1*x
    rects = ax.bar(x, y, width, color='r',label="Arm",edgecolor='black')
    ax.legend(loc='lower center')
    ax.set_xscale('log')
#    xs=np.concatenate(x1,x2)
#    print xs
    ax.set_xticks(x)
    ax.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
    
#    maxy = 1.1*max(m)
#    ax.set_ylim(0, maxy)
    plt.show()



#def getData(rdata,pes,pdata):
#    pes=np.array(rdata[:,0])
#    tdata=np.array(rdata[:,1])
#    print pes
#    print tdata

#    spes=np.array(pes[:]/pes[0])
#    sdata=np.array(tdata[0]/spes[:])
#    
#    pes=np.array(spes[:]/sdata[:])


#plot2Data(lnodes, lpe, unodes, upe, "Scaling")

fname="arm.dat"
adata=np.genfromtxt(fname, skip_header=0, usecols=range(0,2))
#getData(adata,pes,pef)
pes=np.array(adata[:,0])
tdata=np.array(adata[:,1])
spes=np.array(pes[:]/pes[0])
sdata=np.array(tdata[0]/spes[:])
pef=np.array(sdata[:]/tdata[:])

fname="arm_opt.dat"
odata=np.genfromtxt(fname, skip_header=0, usecols=range(0,2))
opes=np.array(odata[:,0])
otdata=np.array(odata[:,1])
ospes=np.array(opes[:]/opes[0])
osdata=np.array(tdata[0]/ospes[:])
opef=np.array(osdata[:]/otdata[:])

plot2Data(pes,pef,opes,opef,"Arm-scale")

#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import sys

def plot2Data(y1 ,y2, y3, y4, xall, fstem):
    fig, ax = plt.subplots()

    line1 = ax.plot(xall, y1, '-o', color='royalblue',label="YAXT 10 timesteps",linewidth=3.0)
    line2 = ax.plot(xall, y2, '-o', color='firebrick',label="ESMF 10 timesteps",linewidth=3.0)
    line3 = ax.plot(xall, y3, '-o', color='seagreen',label="YAXT Routing tables",linewidth=3.0)
    line4 = ax.plot(xall, y4, '-o', color='orange',label="ESMF Routing tables",linewidth=3.0)

    ax.legend(loc='upper left')

    ax.set_xlabel("Number of PEs")
    ax.set_ylabel("Time (s)")

    ax.set_axisbelow(True)
    ax.grid(b=True, which='major', color='lightgray', linestyle='-')

    fname=fstem+".png"
    plt.savefig(fname,format="png")
    fname=fstem+".eps"
    plt.savefig(fname,format="eps")    
    plt.show()


pes = np.array(range(0,5))
pes[0] = 216
pes[1] = 864
pes[2] = 3456
pes[3] = 7776
pes[4] = 13824

yaxt10dt = np.array(range(0,5))
yaxt10dt[0] = 68.38
yaxt10dt[1] = 66.47
yaxt10dt[2] = 72.10
yaxt10dt[3] = 83.21
yaxt10dt[4] = 79.44

yaxtroute = np.array(range(0,5))
yaxtroute[0] = 0.74
yaxtroute[1] = 2.02
yaxtroute[2] = 7.18
yaxtroute[3] = 17.14
yaxtroute[4] = 21.31

esmf10dt = np.array(range(0,5))
esmf10dt[0] = 68.51
esmf10dt[1] = 70.64
esmf10dt[2] = 78.71
esmf10dt[3] = 85.47
esmf10dt[4] = 98.80

esmfroute = np.array(range(0,5))
esmfroute[0] = 6.09
esmfroute[1] = 19.02
esmfroute[2] = 63.26
esmfroute[3] = 138.07
esmfroute[4] = 390.97

plot2Data(yaxt10dt, esmf10dt, yaxtroute, esmfroute, pes,"YAXT-ESMF_weak_scaling")
